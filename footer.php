<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Amethyst
 */

?>

			</div><!-- #content -->

			<footer class="site-footer">
				<?php amethyst_display_social_network_links(); ?>

				<?php wp_nav_menu( array(
						'menu'           => 'Footer', // Do not fall back to first non-empty menu.
						'theme_location' => '__no_such_location',
						'fallback_cb'    => false // Do not fall back to wp_page_menu()
					) ); ?>

				<div class="site-info">
					&copy; <?php echo date('Y'); amethyst_display_copyright_text();  ?>
				</div><!-- .site-info -->
			</footer><!-- .site-footer container-->
		</div><!-- #page -->

		<?php wp_footer(); ?>

		<nav class="off-canvas-container" aria-hidden="true">
		<button type="button" class="off-canvas-close" aria-label="<?php esc_html_e( 'Close Menu', 'amethyst' ); ?>">
			<span class="close"></span>
		</button>

		<?php
			// Mobile menu args.
			$mobile_args = array(
				'theme_location'  => 'mobile',
				'container'       => 'div',
				'container_class' => 'off-canvas-content',
				'container_id'    => '',
				'menu_id'         => 'mobile-menu',
				'menu_class'      => 'mobile-menu',
			);

			// Display the mobile menu.
			wp_nav_menu( $mobile_args );
			?>
		</nav>

		<div class="off-canvas-screen"></div>
	</body>
</html>
