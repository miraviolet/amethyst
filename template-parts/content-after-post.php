<?php
/**
 * The widget area after single post content.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Amethyst
 */

if ( ! is_active_sidebar( 'after-post' ) ) {
	return;
}
?>

<aside class="after-content widget-area col-l-4" role="complementary">
	<?php dynamic_sidebar( 'after-post' ); ?>
</aside><!-- .secondary -->
