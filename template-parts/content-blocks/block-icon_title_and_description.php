<?php
/**
 * The template used for displaying a Icon Title And Description block.
 *
 * @package Amethyst
 */

 // Set up fields.
$title           = get_sub_field( 'title' );
$title_first_word = get_sub_field( 'title_first_word' );
$animation_class = amethyst_get_animation_class();

$icon_1     = get_sub_field( 'icon_1' );
$icon_title_1     = get_sub_field( 'icon_title_1' );
$description_1     = get_sub_field( 'description_1' );

$icon_2     = get_sub_field( 'icon_2' );
$icon_title_2     = get_sub_field( 'icon_title_2' );
$description_2     = get_sub_field( 'description_2' );

$icon_3     = get_sub_field( 'icon_3' );
$icon_title_3    = get_sub_field( 'icon_title_3' );
$description_3     = get_sub_field( 'description_3' );

$icon_4     = get_sub_field( 'icon_4' );
$icon_title_4     = get_sub_field( 'icon_title_4' );
$description_4     = get_sub_field( 'description_4' );

$icon_5     = get_sub_field( 'icon_5' );
$icon_title_5     = get_sub_field( 'icon_title_5' );
$description_5     = get_sub_field( 'description_5' );

// Start a <container> with possible block options.

amethyst_display_block_options(
    array(
        'container' => 'section', // Any HTML5 container: section, div, etc...
        'class'     => 'content-block grid-container icon-title-and-description', // Container class.
    )
);

?>
<section class="content-block  grid-container icon-title-and-description-container container">
    <div class="<?php echo esc_attr( $animation_class ); ?>">
        <div class="icon-title-and-description-header cell">
        <?php if ( $title_first_word ) : ?>
            <h3 class="icon-title-and-description-title"><span class="first-word-light"><?php echo esc_html( $title_first_word ); ?></span>
        <?php endif; ?>

        <?php if ( $title ) : ?>
            <?php echo esc_html( $title ); ?></h3>
        <?php endif; ?>
    </div>

    <div class="icon-title-and-description-wrap">                   
        <div class="cell icon-title-and-description-unit">
            <?php if ( $icon_1 ) : ?>
                <img src="<?php echo $icon_1['url']; ?>" alt="<?php echo $icon_1['alt']; ?>" >
            <?php endif; ?>

            <?php if ( $icon_title_1 ) : ?>
                <span class="icon-title"><?php echo esc_html( $icon_title_1 ); ?></span>
            <?php endif; ?>

            <?php if ( $description_1 ) : ?>
                <span class="description"><?php  echo esc_html( $description_1 ); ?></span>
            <?php endif; ?>
        </div>

        <div class="cell icon-title-and-description-unit">
            <?php if ( $icon_2 ) : ?>
                <img src="<?php echo $icon_2['url']; ?>" alt="<?php echo $icon_2['alt']; ?>" >
            <?php endif; ?>

            <?php if ( $icon_title_2 ) : ?>
                <span class="icon-title"><?php echo esc_html( $icon_title_2 ); ?></span>
            <?php endif; ?>

            <?php if ( $description_2 ) : ?>
                <span class="description"><?php  echo esc_html( $description_2 ); ?></span>
            <?php endif; ?>
        </div>

        <div class="cell icon-title-and-description-unit">
            <?php if ( $icon_3 ) : ?>
                <img src="<?php echo $icon_3['url']; ?>" alt="<?php echo $icon_3['alt']; ?>" >
            <?php endif; ?>

            <?php if ( $icon_title_3 ) : ?>
                <span class="icon-title"><?php echo esc_html( $icon_title_3 ); ?></span>
            <?php endif; ?>

            <?php if ( $description_3 ) : ?>
                <span class="description"><?php  echo esc_html( $description_3 ); ?></span>
            <?php endif; ?>
        </div>

        <div class="cell icon-title-and-description-unit">
            <?php if ( $icon_4 ) : ?>
                <img src="<?php echo $icon_4['url']; ?>" alt="<?php echo $icon_4['alt']; ?>" >
            <?php endif; ?>

            <?php if ( $icon_title_4 ) : ?>
                <span class="icon-title"><?php echo esc_html( $icon_title_4 ); ?></span>
            <?php endif; ?>

            <?php if ( $description_4 ) : ?>
                <span class="description"><?php  echo esc_html( $description_4 ); ?></span>
            <?php endif; ?>
        </div>

        <div class="cell icon-title-and-description-unit">
            <?php if ( $icon_5 ) : ?>
                <img src="<?php echo $icon_5['url']; ?>" alt="<?php echo $icon_5['alt']; ?>" >
            <?php endif; ?>

            <?php if ( $icon_title_5 ) : ?>
                <span class="icon-title"><?php echo esc_html( $icon_title_5 ); ?></span>
            <?php endif; ?>

            <?php if ( $description_5 ) : ?>
                <span class="description"><?php  echo esc_html( $description_5 ); ?></span>
            <?php endif; ?>
        </div>
    </div><!-- .grid-x -->
 </section>
 </section>