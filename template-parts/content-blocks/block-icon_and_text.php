<?php
/**
 * The template used for displaying a Icon And Text block.
 *
 * @package Amethyst
 *
 */

 // Set up fields.
$icon_and_text     = get_sub_field( 'icon_and_text' );
$title           = get_sub_field( 'title' );
$title_first_word  = get_sub_field( 'title_first_word' );
$button_url      = get_sub_field( 'button_url' );
$button_text     = get_sub_field( 'button_text' );
$animation_class = amethyst_get_animation_class();

$text1            = get_sub_field( 'text1' );
$icon1           = get_sub_field( 'icon1' );

$text2            = get_sub_field( 'text2' );
$icon2           = get_sub_field( 'icon2' );

$text3            = get_sub_field( 'text3' );
$icon3           = get_sub_field( 'icon3' );

$text4            = get_sub_field( 'text4' );
$icon4           = get_sub_field( 'icon4' );
// Start a <container> with possible block options.
amethyst_display_block_options(
    array(
        'container' => 'section', // Any HTML5 container: section, div, etc...
        'class'     => 'content-block grid-container icon-and-text', // Container class.
    )
);

?>
<section class="content-block  grid-container icon-and-text-container container">
    <div class="<?php echo esc_attr( $animation_class ); ?>">
        <div class="icon-and-text-header cell">
        <?php if ( $title_first_word ) : ?>
            <h3 class="icon-and-text-title"><span class="first-word-dark"><?php echo esc_html( $title_first_word ); ?></span>
        <?php endif; ?>

        <?php if ( $title ) : ?>
            <?php echo esc_html( $title ); ?></h3>
        <?php endif; ?>
        </div>

        <div class="icon-and-text-wrap">
            <div class="cell icon-and-text-repeater">
                <?php if ( $icon1 ) : ?>
                    <img src="<?php echo $icon1['url']; ?>" alt="<?php echo $icon1['alt']; ?>">
                <?php endif; ?>

                <?php if ( $text1 ) : ?>
                    <span class="text"><?php echo esc_html( $text1 ); ?></span>
                <?php endif; ?>
            </div>

            <div class="cell icon-and-text-repeater">
                <?php if ( $icon2 ) : ?>
                    <img src="<?php echo $icon2['url']; ?>" alt="<?php echo $icon2['alt']; ?>">
                <?php endif; ?>

                <?php if ( $text2 ) : ?>
                    <span class="text"><?php echo esc_html( $text2 ); ?></span>
                <?php endif; ?>
            </div>

            <div class="cell icon-and-text-repeater">
                <?php if ( $icon3 ) : ?>
                    <img src="<?php echo $icon3['url']; ?>" alt="<?php echo $icon3['alt']; ?>">
                <?php endif; ?>

                <?php if ( $text3 ) : ?>
                    <span class="text"><?php echo esc_html( $text3 ); ?></span>
                <?php endif; ?>
            </div>

            <div class="cell icon-and-text-repeater">
                <?php if ( $icon4 ) : ?>
                    <img src="<?php echo $icon4['url']; ?>" alt="<?php echo $icon4['alt']; ?>">
                <?php endif; ?>

                <?php if ( $text4 ) : ?>
                    <span class="text"><?php echo esc_html( $text4 ); ?></span>
                <?php endif; ?>
            </div>
        </div>

        <div class="button-container cell">
            <?php if ( $button_url ) : ?>
                <button type="button" class="button cta-button" onclick="location.href='<?php echo esc_url( $button_url ); ?>'"><?php echo esc_html( $button_text ); ?></button>
            <?php endif; ?>
        </div>
    </div><!-- .grid-x -->
 </section>
</section>