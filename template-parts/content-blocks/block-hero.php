<?php
/**
 * The template used for displaying a hero.
 *
 * @package Amethyst
 */

// Set up fields.
$hero        = get_sub_field( 'hero_slides' );
$slide_count = count( $hero );

// Start repeater markup...
if ( have_rows( 'hero_slides' ) ) :

	// If there is more than one slide...
	if ( $slide_count > 1 ) :
		echo '<section class="content-block container carousel">';

		// Enqueue Slick.
		wp_enqueue_style( 'slick-carousel' );
		wp_enqueue_script( 'slick-carousel' );
	endif;

	// Loop through hero(s).
	while ( have_rows( 'hero_slides' ) ) :
		the_row();

		// Set up fields.
		$first_word           = get_sub_field( 'first_word' );
		$title           = get_sub_field( 'headline' );
		$text            = get_sub_field( 'text' );
		$text2            = get_sub_field( 'text2' );
		$button_text     = get_sub_field( 'button_text' );
		$button_url      = get_sub_field( 'button_url' );
		$animation_class = amethyst_get_animation_class();
		$other_options = get_sub_field( 'other_options' ) ? get_sub_field( 'other_options' ) : get_field( 'other_options' )['other_options'];

		// Start a <container> with possible block options.
		amethyst_display_block_options( array(
			'container' => 'section', // Any HTML5 container: section, div, etc...
			'class'     => 'content-block hero slide', // Container class.
		) );

		// If we have a slider, set the animation in a data-attribute.
		if ( $slide_count > 1 ) : ?>
			<div class="hero-content" data-animation="<?php echo esc_attr( $animation_class ); ?>">
		<?php else : ?>
			<div class="hero-content <?php echo esc_attr( $animation_class ); ?>">

			<div class="wrap">
		<?php endif; ?>

			<?php if ( $title ) : ?>
				<h1 class="hero-title"><?php if ( $first_word ) : ?>
				<span class="first-word" style="box-shadow: <?php echo esc_html( $other_options['font_color'] ); ?>;"><?php echo esc_html( $first_word ); ?></span>
			<?php endif; ?><?php echo esc_html( $title ); ?></h1>
			<?php endif; ?>

			<?php if ( $text ) : ?>
				<p class="hero-description"><?php echo esc_html( $text ); ?></p>
			<?php endif; ?>

			<?php if ( $text2 ) : ?>
				<p class="hero-description-2"><?php echo esc_html( $text2 ); ?></p>
			<?php endif; ?>
			</div>
			<?php if ( $button_url ) : ?>
			<div class="button-wrap">
				<button type="button" class="button button-hero" onclick="location.href='<?php echo esc_url( $button_url ); ?>'"><?php echo esc_html( $button_text ); ?></button>
			</div>
			<?php endif; ?>

			
		</div><!-- .hero-content -->
	</section><!-- .hero -->

<?php
	endwhile;

	if ( $slide_count > 1 ) :
		echo '</section>';
	endif;

endif;
?>
