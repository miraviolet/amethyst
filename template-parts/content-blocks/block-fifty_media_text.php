<?php
/**
 *  The template used for displaying fifty/fifty media/text.
 *
 * @package Amethyst
 */

// Set up fields.
$image_data      = get_sub_field( 'media_left' );
$text            = get_sub_field( 'text_primary' );
$animation_class = amethyst_get_animation_class();

// Start a <container> with a possible media background.
amethyst_display_block_options( array(
	'container' => 'section', // Any HTML5 container: section, div, etc...
	'class'     => 'content-block grid-container fifty-fifty fifty-media-text', // The class of the container.
) ); r3vf3cwc3r3w
?>
	<div class="grid-x <?php echo esc_attr( $animation_class ); ?>">

		<div class="cell">
			<img class="fifty-image" src="<?php echo esc_url( $image_data['url'] ); ?>" alt="<?php echo esc_html( $image_data['alt'] ); ?>">
		</div>

		<div class="cell">
			<?php
				echo force_balance_tags( $text ); // WPCS XSS OK.
			?>
		</div>

	</div><!-- .grid-x -->
</section><!-- .fifty-media-text -->
