<?php
/**
 * Template part for displaying default posts in a feed.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Amethyst
 */
 $category_color = get_field('category_color');

 $category = get_the_category();
?>

<article <?php post_class('card'); ?>>
	<?php if (has_post_thumbnail()) { ?>
	<figure class="featured-image index-image">
		<a href="<?php echo esc_url(get_permalink()) ?>" rel="bookmark">
			<?php the_post_thumbnail('archive_thumbnail'); ?>
		</a>
	</figure><!-- .featured-image full-bleed -->
	<?php } ?>

	<header class="entry-header">
		<!-- CATEGORY START -->
		<a href="<?php echo get_category_link($category[0]->term_id ); ?>" title="Category Name">
			<span class="category cat <?php the_field('category_color'); ?>">
				<?php echo $category[0]->cat_name; ?>
			</span>
		</a>
		<!-- CATEGORY END -->

		<?php

		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );

		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );

		endif;

		if ( 'post' === get_post_type() ) :

		?>
		
		<div class="entry-meta">
			<?php //amethyst_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_excerpt($post->post_content, get_the_excerpt()); ?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php // amethyst_entry_footer(); ?>

		<span class="author">
			<?php echo get_avatar(get_the_author_meta('ID'), 32); ?>
			<span class="author-by-line">By, </span>
			<span class="author-name"><?php the_author_meta( 'nickname' ); ?></span>
		</span> <!-- .author -->
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->