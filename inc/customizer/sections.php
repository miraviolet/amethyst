<?php
/**
 * Customizer sections.
 *
 * @package Amethyst
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function amethyst_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'amethyst_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'amethyst' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'amethyst_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'amethyst' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'amethyst' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'amethyst_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'amethyst' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'amethyst_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'amethyst' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'amethyst_customize_sections' );
