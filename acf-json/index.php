<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Amethyst
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
